n=10;
m=2^n;

%Model Combinations
tt = dec2bin(0:2^n-1)-'0';

predictors = [data2.CPI, data2.GDP, data2.GDP_t1, data2.GDP_t2, data2.M1, data2.Loans_B,  ...
    data2.Rate_FED, data2.Rate_T10, data2.ExJPUS, data2.ExUKUS];

pred_list = ["CPI" "GDP" "GDP_t1" "GDP_t2" "M1" "Loans_B"  ...
    "Rate_FED" "Rate_T10" "ExJPUS" "ExUKUS"];

Y = data2.Unemp;

%Allocate space
r2_vec = zeros(m,1);
bic_vec = zeros(m,1);

for i=1:m
    X_best = tt(i,:).*predictors;
    X_best( :, ~any(X_best,1) ) = [];
    mdl = fitlm(X_best,Y,'RobustOpts','ols');
    r2_vec(i) = mdl.Rsquared.Adjusted;
    bic_vec(i) = mdl.ModelCriterion.BIC;
end

%Model probabilities
mdl_prob = zeros(m,1);
for j=1:m
    mdl_prob(j) = bic_vec(j)/sum(bic_vec);
end

%Variable specific probabilities
var_prob = sum(tt.*mdl_prob,1);

%Model averaging
X_avg = var_prob.*predictors;
mdl_avg = fitlm(X_avg,Y,'RobustOpts','ols');
r2_avg = mdl_avg.Rsquared.Adjusted;
bic_avg = mdl_avg.ModelCriterion.BIC;

%Model selection
[max_prob, max_index] = max(mdl_prob)

%Results
disp("Weighted average model:");
disp("Adjusted R-Squared: " + r2_avg);
disp("BIC: " + bic_avg);

best_pred = pred_list(logical(tt(max_index,:)));
str = "Unemployment = f(";
for i=1:(size(best_pred,2)-1)
    str = str + best_pred(i) + ", ";
end
str = str + best_pred(end) + ")";

disp("The model with the highest probability of being the �best� model: " + str );
disp("Adjusted R-Squared: " + r2_vec(max_index));
disp("BIC: "+ bic_vec(max_index));


disp("Estimation of a linear model using the best model obtained above:");
X_best = tt(max_index,:).*predictors;
X_best( :, ~any(X_best,1) ) = [];
mdl = fitlm(X_best,data2.Unemp,'RobustOpts','ols')


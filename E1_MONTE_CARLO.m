
% % Settings for Monte Carlo exercise
% n = 200;    Number of data observations
% rho = 0.9;    Correlation coefficient (keep between -1 and 1)
% p = 2;      Number of predictors
% sigma2 = 0.5;    Regression variance
% beta = [1.3; 0.9];

%Original inputs
MONTE_CARLO(200, 0.9, 2, 0.5, [1.3; 0.9])

%Changing n=10000
MONTE_CARLO(10000, 0.9, 2, 0.5, [1.3; 0.9])

%Changing rho = 0.1
MONTE_CARLO(200, 0.1, 2, 0.5, [1.3; 0.9])

%Changing sigma2 = 50
MONTE_CARLO(200, 0.9, 2, 50, [1.3; 0.9])

%Changing beta = [3.9; 2.7]
MONTE_CARLO(200, 0.9, 2, 0.5, [3.9; 2.7])

%Changing p=3, beta = [[1.3; 0.9; 2.1]]
MONTE_CARLO(200, 0.9, 3, 0.5, [1.3; 0.9; 2.1])


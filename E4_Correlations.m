%Initial analysis of the variables we think may have a relationship with
%the rate of unemployment in the US

%Scatter plot of first group of variables and Unemployment
tiledlayout(3,3)
nexttile
scatter(data2.Unemp, data2.CPI)
xlabel('CPI')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.GDP)
xlabel('GDP')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.Wage)
xlabel('Wage')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.M0)
xlabel('M0')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.M1)
xlabel('M1')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.Loans_B)
xlabel('Business Loans')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.Loans_C)
xlabel('Commercial Loans')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.Rate_FED)
xlabel('Interest Rate FED')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.Rate_T10)
xlabel('Interest Rate T10')
ylabel('Unemployment')

%Scatter plot of second group of variables and Unemployment
tiledlayout(3,3)
nexttile
scatter(data2.Unemp, data2.Oil)
xlabel('Oil Price')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.ExCAUS)
xlabel('Canada-US Forex')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.ExJPUS)
xlabel('Japan-US Forex')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.ExEUUS)
xlabel('Euro-US Forex')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.ExUKUS)
xlabel('UK-US Forex')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.Senti)
xlabel('Consumer Sentiment')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.CPI_t1)
xlabel('CPI t\_1')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.GDP_t1)
xlabel('GDP t\_1')
ylabel('Unemployment')
nexttile
scatter(data2.Unemp, data2.CPI_t2)
xlabel('CPI t\_2')
ylabel('Unemployment')


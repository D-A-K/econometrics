%Focus on the variable we are trying to explain
%Graph a correlogram of the time series to check the number of lags
autocorr(data2.Unemp,'NumLags',10)

%Estimate an AR(10) model
ar(data.Unemp,10)

%Use CPI as an explanatory variable - Philips Curve
indep = [data2.CPI];
mdl = fitlm(indep,data2.Unemp,'RobustOpts','ols')

indep = [data2.CPI, data2.GDP, data2.Wage, data2.M0, data2.M1, data2.Loans_B, ...
    data2.Loans_C, data2.Rate_FED, data2.Rate_T10, data2.Oil, data2.ExCAUS, ...
    data2.ExJPUS, data2.ExEUUS, data2.ExUKUS, data2.Senti];
mdl = fitlm(indep,data2.Unemp,'RobustOpts','ols')

indep = [data2.CPI, data2.GDP, data2.Loans_B,  ...
    data2.Rate_FED, data2.Rate_T10, data2.ExJPUS, data2.ExUKUS];
mdl = fitlm(indep,data2.Unemp,'RobustOpts','ols')

indep = [data2.CPI, data2.GDP, data2.GDP_t1, data2.GDP_t2, data2.M1, data2.Loans_B,  ...
    data2.Rate_FED, data2.Rate_T10, data2.ExJPUS, data2.ExUKUS];
mdl = fitlm(indep,data2.Unemp,'RobustOpts','ols')

tiledlayout(2,2)
nexttile
plotResiduals(mdl)
nexttile
plotResiduals(mdl,'probability')
nexttile
plotResiduals(mdl,'fitted')
nexttile
plotResiduals(mdl,'lagged')
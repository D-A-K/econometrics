function deltaData = Delta(data)

deltaData = zeros((size(data,1)-1),1);
for i=2:size(data)
    deltaData(i-1)=data(i)-data(i-1);
end
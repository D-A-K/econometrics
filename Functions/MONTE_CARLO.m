function MONTE_CARLO(n, rho, p ,sigma2, beta)
M = 100;

estimates = zeros(M,2*p);
% b_OLS_p1 = zeros(M, 1);
% b_OLS_p2 = zeros(M, 1);
% b_BIASED_p1 = zeros(M, 1);
% b_BIASED_p2 = zeros(M, 1);
for i = 1:M
        betaTable = MONTE_CARLO_bias(n, rho, p ,sigma2, beta);
        for j = 1:size(betaTable, 2)
            estimates(i,j) = betaTable(j);
%         b_OLS_p2(i,1) = betaTable(2); %accessing OLS estimation for second predictor   
%         b_BIASED_p1(i,1) = betaTable(3); %accessing BIASED estimation for first predictor
%         b_BIASED_p2(i,1) = betaTable(4); %accessing BIASED estimation for second predictor
        end
end

averages = mean(estimates);

TRUE=beta;
OLS = averages(1,1:p).';
BIASED = averages(1,p+1:2*p).';

disp(table(TRUE, OLS, BIASED ))





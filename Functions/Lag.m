function lagdata = Lag(data, steps)

n = size(data,1);
lagdata = zeros(n,1);
lagdata(1+steps:n) = data(1:n-steps);

%Treatment of quarterly observations for GDP
x = zeros(292,1);
x(1)=2;
for i=2:292
    x(i)=x(i-1)+3;
end

xx = zeros(876,1);
xx(1)=2;
for i=2:876
    xx(i)=xx(i-1)+1;
end

GDPQ = readtable('GDPQ.xlsx');

y = table2array(GDPQ(:,2));
yy = spline(x,y,xx);

%Monthly dates
d1 = datetime(1946,12,1);
t1 = datetime(d1,'Format','dd-MM-yyyy');
Date = t1 + calmonths(1:size(yy));
Date = Date.';
GDP_M = yy;

GDPM = table(Date, GDP_M);

%Check the approximation of quarterly data to monthly observations
datetick('x', 'yyyy QQ')
scatter(GDPQ.Date(year(GDPQ.Date)>=2005 & year(GDPQ.Date)<=2010), GDPQ.GDP_Q(year(GDPQ.Date)>=2005 & year(GDPQ.Date)<=2010))
hold on
plot(GDPM.Date(year(GDPM.Date)>=2005 & year(GDPM.Date)<=2010), GDPM.GDP_M(year(GDPM.Date)>=2005 & year(GDPM.Date)<=2010))
title('Monthly vs Quarterly GDP 2005 to 2010')

%Data in one table
data = readtable('Data.xlsx','Sheet','Monthly');

%Replace NaN
data.ExUSEU(isnan(data.ExUSEU))=0;

%Conversions
ExUKUS = zeros(size(data,1),1);
ExUKUS(data.ExUSEU ~=0 ) = 1./data.ExUSEU(data.ExUSEU ~=0);
data.ExEUUS = ExUKUS;

ExUKUS = zeros(size(data,1),1);
ExUKUS(data.ExUSUK ~=0 ) = 1./data.ExUSUK(data.ExUSUK ~=0);
data.ExUKUS = ExUKUS;

%Add Monthly GDP and Lag variables
data.GDP = GDPM.GDP_M;
data.GDP_t1 = Lag(data.GDP, 1);
data.GDP_t2 = Lag(data.GDP, 2);
data.CPI_t1 = Lag(data.CPI, 1);
data.CPI_t2 = Lag(data.CPI, 2);

%Crop tables
startDate = datetime(1971,1,1); 
endDate = datetime(2019,12,1);
data = data(data.Date >= startDate & data.Date <= endDate, :);


%Take first differences to solve problem of non stationarity
%We first addressed the problem in 'Stationarity.m' and constructed the function 'Delta' to create 'data2' 
Date = data.Date(2:size(data.Date)); %We lose one observation 
data2 = table(Date);
data2.Unemp = Delta(data.Unemp);
data2.CPI = Delta(data.CPI);
data2.GDP = Delta(data.GDP);
data2.Wage = Delta(data.Wage);
data2.M0 = Delta(data.M0);
data2.M1 = Delta(data.M1);
data2.Loans_B = Delta(data.Loans_B);
data2.Loans_C = Delta(data.Loans_C);
data2.Rate_FED = Delta(data.Rate_FED);
data2.Rate_T10 = Delta(data.Rate_T10);
data2.Oil = Delta(data.Oil);
data2.ExCAUS = Delta(data.ExCAUS);
data2.ExJPUS = Delta(data.ExJPUS);
data2.ExEUUS = Delta(data.ExEUUS);
data2.ExUKUS = Delta(data.ExUKUS);
data2.Senti = Delta(data.Senti);
data2.CPI_t1 = Delta(data.CPI_t1);
data2.CPI_t2 = Delta(data.CPI_t2);
data2.GDP_t1 = Delta(data.GDP_t1);
data2.GDP_t2 = Delta(data.GDP_t2);



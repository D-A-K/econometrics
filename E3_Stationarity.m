tiledlayout(4,4)
nexttile
plot(data.Date, data.Unemp)
title("Unemployment")
nexttile
plot(data.Date, data.CPI)
title("CPI")
nexttile
plot(data.Date, data.GDP)
title("GDP")
nexttile
plot(data.Date, data.Wage)
title("Wage")
nexttile
plot(data.Date, data.M0)
title("M0")
nexttile
plot(data.Date, data.M1)
title("M1")
nexttile
plot(data.Date, data.Loans_B)
title("Business Loans")
nexttile
plot(data.Date, data.Loans_C)
title("Commercial Loans")
nexttile
plot(data.Date, data.Rate_FED)
title("Interest Rate FED")
nexttile
plot(data.Date, data.Rate_T10)
title("Interest Rate T-10")
nexttile
plot(data.Date, data.Oil)
title("Oil Price")
nexttile
plot(data.Date, data.ExCAUS)
title("Canada-US Forex")
nexttile
plot(data.Date, data.ExJPUS)
title("Japan-US Forex")
nexttile
plot(data.Date, data.ExEUUS)
title("Euro-US Forex")
nexttile
plot(data.Date, data.ExUKUS)
title("UK-US Forex")
nexttile
plot(data.Date, data.Senti)
title("Consumer Sentiment")

adf = zeros(16,2);
indep_vars = ["Unemp" "CPI" "GDP" "Wage" "M0" "M1" "Loans_B" "Loans_C" ...
    "Rate_FED" "Rate_T10" "Oil" "ExCAUS" "ExJPUS" "ExEUUS" "ExUKUS" "Senti"].';
for i=1:size(indep_vars,1)
    st = indep_vars(i);
    adf(i,1) = adftest(data.(st));
    adf(i,2) = adftest(data2.(st));
end

levels = adf(:,1);
diff = adf(:,2);
adf_table = table(indep_vars,levels,diff);
disp(adf_table)

tiledlayout(4,4)
nexttile
plot(data2.Date, data2.Unemp)
title("Unemployment")
nexttile
plot(data2.Date, data2.CPI)
title("CPI")
nexttile
plot(data2.Date, data2.GDP)
title("GDP")
nexttile
plot(data2.Date, data2.Wage)
title("Wage")
nexttile
plot(data2.Date, data2.M0)
title("M0")
nexttile
plot(data2.Date, data2.M1)
title("M1")
nexttile
plot(data2.Date, data2.Loans_B)
title("Business Loans")
nexttile
plot(data2.Date, data2.Loans_C)
title("Commercial Loans")
nexttile
plot(data2.Date, data2.Rate_FED)
title("Interest Rate FED")
nexttile
plot(data2.Date, data2.Rate_T10)
title("Interest Rate T-10")
nexttile
plot(data2.Date, data2.Oil)
title("Oil Price")
nexttile
plot(data2.Date, data2.ExCAUS)
title("Canada-US Forex")
nexttile
plot(data2.Date, data2.ExJPUS)
title("Japan-US Forex")
nexttile
plot(data2.Date, data2.ExEUUS)
title("Euro-US Forex")
nexttile
plot(data2.Date, data2.ExUKUS)
title("UK-US Forex")
nexttile
plot(data2.Date, data2.Senti)
title("Consumer Sentiment")



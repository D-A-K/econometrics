%Complete data
X_lasso = table2array(data2(:,3:21));
[B,FitInfo] = lasso(X_lasso,Y);

%When p>n
X_lasso = table2array(data2(1:10,3:21));
[B,FitInfo] = lasso(X_lasso,Y(1:10,:));

%preselected variables
X_lasso = predictors(1:10, :);
[B,FitInfo] = lasso(X_lasso,Y(1:10,:));

%Subsamples of data
X_lasso = table2array(data2(1:294,3:21));
[B1,FitInfo] = lasso(X_lasso,Y(1:294,:));

X_lasso = table2array(data2(295:end,3:21));
[B2,FitInfo] = lasso(X_lasso,Y(295:end,:));

B_mean = (B1+B2)/2;

%odd rows
X_lasso = table2array(data2(1:2:end,3:21));
[B_odd,FitInfo] = lasso(X_lasso,Y(1:2:end,:));

%even rows
X_lasso = table2array(data2(2:2:end,3:21));
[B_even,FitInfo] = lasso(X_lasso,Y(2:2:end,:));

B_mean2 = (B_odd + B_even)/2;
